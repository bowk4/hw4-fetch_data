function sendAjaxRequest(url, callback) {
    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => callback(data))
        .catch(error => console.error('Fetch error:', error));
}

sendAjaxRequest("https://ajax.test-danit.com/api/swapi/films", function (filmsData) {
    const charactersContainer = document.querySelector(".list__characters");

    filmsData.forEach(function (film) {
        const filmWrapper = document.createElement("div");
        filmWrapper.className = "list__characters-title-wrapper";
        const filmTitle = document.createElement('h2');
        filmTitle.className = "list__characters-title";
        filmTitle.style.cssText = "font-size: 18px; min-height: 50px; margin-bottom: 15px"
        filmTitle.textContent = "Episode " + film.episodeId + ": " + film.name + film.openingCrawl;

        const characterList = document.createElement('ul');
        characterList.style.listStyle = "none"

        filmWrapper.appendChild(filmTitle);
        filmWrapper.appendChild(characterList);
        charactersContainer.appendChild(filmWrapper);

        getCharactersInfo(film.characters, characterList)
    })
});

function getCharactersInfo(characterUrls, characterList) {
    characterList.innerHTML = "";

    characterUrls.forEach(function (characterUrl) {
        sendAjaxRequest(characterUrl, function (characterData) {
            const characterName = characterData.name;

            const characterListItem = document.createElement("li");
            characterListItem.className = "list__characters-text";
            characterListItem.style.marginBottom = "10px";
            characterListItem.style.fontSize = "14px";
            characterListItem.innerHTML = "Name: " + characterName;
            characterList.appendChild(characterListItem);
        });
    })
}
